---
title: "Frama"
order: 4
in_menu: true
---
<article class="framalibre-notice">
    <div>
      <img src="https://framalibre.org/images/logo/Organic%20Maps.svg">
    </div>
    <div>
      <h2>Organic Maps</h2>
      <p>Application libre de cartes hors ligne et navigation GPS basée sur les données OpenStreetMap - Pour Android et iOS</p>
      <div>
        <a href="https://framalibre.org/notices/organic-maps.html">Vers la notice Framalibre</a>
        <a href="https://organicmaps.app/fr/">Vers le site</a>
      </div>
    </div>
  </article> 